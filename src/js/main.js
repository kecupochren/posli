var page = {
	_mobile: false,
	
	init: function() {
		if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			page._mobile = true;	
		}

		// jquery placeholder plugin for ie9
		$('input, textarea').placeholder();

		// wow.js - animations on scroll library
		new WOW().init();

		page.bindScrollEvents();

		page.bindNavigationItems();

		page.bindClickEvents();

		page.replaceSvgs();

		page.hoverNavigationFix();

		page.handleDummyForm();
	},
	
	bindScrollEvents: function() {
		var lastScrollTop = 0;
		var didScroll = false;
		var signup = document.getElementById('signup');

		$(window).bind('scroll', function() {
			didScroll = true;
		});

		// scroll event throttle
		setInterval(function() {
			if (didScroll) {
				didScroll = false;

				var navigation_height = $('.navigation').height();

				var about_offset = $('.about').offset().top;
				var task_management_offset = $('.task-management').offset().top - navigation_height;
				var signup_offset = $('.features').offset().top + parseInt($('.features').css('padding-top'));

				var top = page.getScroll();

				// scroll down
				if (top > lastScrollTop) {
					if (page.isElementVisible(signup)) {
						page.setActiveNavItem('#signup');
					}

					else if (top >= task_management_offset) { 
						page.setActiveNavItem('#task-management');
					}

					if (page._mobile) {
						if (top >= about_offset - navigation_height) { 
							$('.navigation').addClass('navigation--fixed');
						}
					} else {
						if (top >= about_offset) { 
							$('.navigation').addClass('navigation--fixed');
						}
					}
				} 

				// scroll up
				else {
					if (!page.isElementVisible(signup)) {
						page.setActiveNavItem('#task-management');
					}

					if (top < task_management_offset) {
						page.setActiveNavItem('#about');
					}

					if (top < about_offset - navigation_height) {
						$('.navigation').removeClass('navigation--fixed');
					}
				}

				lastScrollTop = top;
			}
		}, 250);
	},
	
	bindNavigationItems: function() {
		$('.navigation__item').on('click', function(e) {
			e.preventDefault();
			
			var offset = Math.ceil($($(this).attr('href')).offset().top);

			if (($(window).width() >= 1440) && (page.getScroll() < $('.about').offset().top)) {
				offset -= 65;
			} else {
				offset -= $('.navigation').outerHeight();
			}

			$("html, body").animate({ scrollTop: offset}, 500);
		});
	},

	bindClickEvents: function() {
		$('.about__btn').on('click', function(e) {
			e.preventDefault();

			$("html, body").animate({ scrollTop: 0}, 500);
		});
	},
	
	replaceSvgs: function() {
		$('div.media--svg').each(function() {
			var _self = this;
			$.get($(this).attr('data-svg'), function(data) {
				var svg = $(data).find('svg');
				
				svg = svg.removeAttr('xmlns:a');
				
				$(_self).html(svg);
			}, 'xml');
			
			$(this).removeClass('tosvg');
		});			
	},
	
	setActiveNavItem: function(item) {
		$('.navigation__item').removeClass('navigation__item--active');					

		$('.navigation__item[href="' + item + '"]').addClass('navigation__item--active');	
	},
	
	handleDummyForm: function() {
		$('#email-form--header, #email-form--footer').parsley();

		$('.email-form').on('submit', function(e) {
			e.preventDefault();
		});

		window.Parsley.on('form:success', function() {
			var element = $(this)[0].$element;

		  	element.css('opacity', 0);

		  	element.parent().addClass('signup-form--success');

		  	element.parent().parent().addClass('signup-form--success');

		  	setTimeout(function() {
		  		element.parent().find('.email-success').css({
		  			'opacity': 1,
		  			'display': 'block'
		  		});
		  	},150);
		});		
	},

	hoverNavigationFix: function() {
		$('.navigation__item').hover(function(){
			$('.navigation__items').toggleClass('navigation__items--hover');
		});
	},
	
	getScroll: function() {
		return $('html').scrollTop() || $('body').scrollTop() || $(window).scrollTop();
	},
	
	isElementVisible: function(element) {
		var rect     = element.getBoundingClientRect(),
			vWidth   = window.innerWidth || doc.documentElement.clientWidth,
			vHeight  = window.innerHeight || doc.documentElement.clientHeight,
			efp      = function (x, y) { return document.elementFromPoint(x, y) };     

		// return false if it's not in the viewport
		if (rect.right < 0 || rect.bottom < 0 
				|| rect.left > vWidth || rect.top > vHeight)
			return false;

		// return true if any of its four corners are visible
		return (
			  element.contains(efp(rect.left,  rect.top))
		  ||  element.contains(efp(rect.right, rect.top))
		  ||  element.contains(efp(rect.right, rect.bottom))
		  ||  element.contains(efp(rect.left,  rect.bottom))
		);
	}
};

$(document).ready(function() {
	page.init();
});