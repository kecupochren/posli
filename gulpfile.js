var gulp            = require('gulp');
var browserSync     = require('browser-sync').create();
var sass            = require('gulp-sass');
var concat			= require('gulp-concat');
var uglify			= require('gulp-uglify');
var autoprefixer    = require('gulp-autoprefixer');
var cssnano 		= require('gulp-cssnano');

function onError(err) {
  console.log(err);
  this.emit('end');
}

gulp.task('serve', ['sass'], function() {

	browserSync.init({
		server: "./"
	});

	gulp.watch("src/sass/**/*.scss", ['sass', 'sass-ie']);
	gulp.watch("src/js/libs/*.js", ['libs']);
	gulp.watch("src/js/main.js", ['js']);
	gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
	return gulp.src("src/sass/main.scss")
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(cssnano())
		.pipe(gulp.dest("css"))
		.on('error', onError)
		.pipe(browserSync.stream());
});

gulp.task('sass-ie', function() {
	return gulp.src("src/sass/ie.scss")
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['IE 9'],
			cascade: false
		}))
		.pipe(cssnano())
		.pipe(gulp.dest("css"))
		.on('error', onError)
		.pipe(browserSync.stream());
});

gulp.task('libs', function() {
	return gulp.src([
			'src/js/libs/jquery.js',
			'src/js/libs/modernizr.js',
			'src/js/libs/picturefill.js',
			'src/js/libs/jquery.placeholder.js',
			'src/js/libs/parsley.js',
			'src/js/libs/wow.js',
			'src/js/libs/smoothscroll.js' // the best smooth scroll library I know of
		])
		.pipe(concat('libs.js'))
		.pipe(uglify())
		.pipe(gulp.dest('js'))
});

gulp.task('js', function() {
	return gulp.src("src/js/main.js")
		.pipe(uglify())
		.on('error', onError)
		.pipe(gulp.dest('js'))
})

gulp.task('default', ['serve']);